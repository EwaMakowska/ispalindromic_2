﻿using System;
using System.Linq;

namespace PalindromicNumbes_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum = 0;
            int max = 1000000;

            for(int i = 0; i < max; i++)
            {
                if (isPalindromic(i))
                {
                    sum += i;
                    Console.WriteLine(sum + "+" + i + "="+(sum + i));
                }
            }

            Console.WriteLine("RESULT: " + sum);

        }

        public static Boolean isPalindromic(int number)
        {
            if (number.ToString().Equals(reversedNumber(number)))
            {
                return true;
            }
            return false;
        }

        public static String reversedNumber(int number)
        {
            String numberAsString = number.ToString();
            char[] numberAsChartArray = numberAsString.ToCharArray();
            char[] numberReverseArray = (char[])numberAsChartArray.Clone();
            Array.Reverse(numberReverseArray);
            return new String(numberReverseArray);
        }
    }
}
